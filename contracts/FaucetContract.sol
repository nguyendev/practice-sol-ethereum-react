pragma solidity >=0.4.22 <0.9.0;

import "./Owned.sol";
import "./Logger.sol";
import "./IFaucet.sol";

contract Faucet is Owned, Logger, IFaucet {
  uint public numOfFunders;
  
  mapping(address => bool) private funders;
  mapping(uint => address) private lutFunders;

  modifier limitWithdraw(uint withdrawAmount) {
        require(
          withdrawAmount < 10000000000000000000, 
          "Cannot withdraw more than 0.1 ether"
        );
        _;
  }

  receive() external payable {}

  function emitLog() public override pure returns(bytes32) {
      return "Hello world";
  }

  function addFunds() override external payable {
    address funder = msg.sender;

    if (!funders[funder]) {
      numOfFunders++;
      uint index = numOfFunders++;
      funders[funder] = true;
      lutFunders[index] = funder;
    }
  }

  function withdraw(uint withdrawAmount) override external limitWithdraw(withdrawAmount) {
    payable(msg.sender).transfer(withdrawAmount);  
  }

  function getAllFunders() external view returns (address[] memory) {
    address[] memory _funders = new address[](numOfFunders);

    for (uint i = 0; i < numOfFunders; i++) {
      _funders[i] = lutFunders[i];
    }

    return _funders;
  }

  function getFunderAtIndex(uint8 index) external view returns(address) {
    return lutFunders[index];
  }

  function test1() external onlyOwner() {

  }

  function test2() external onlyOwner() {

  }
}


// const instance = await Faucet.deployed();
// instance.addFunds({from: accounts[0], value: "200000000"})
// instance.addFunds({from: accounts[1], value: "200000000"})

// instance.addFunds({from: accounts[0], value: "2000000000000000000"})
// instance.addFunds({from: accounts[1], value: "2000000000000000000"})

// instance.withdraw("500000000000000000", {from: accounts[1]})

// instance.getFunderAtIndex(0)
// instance.getAllFunders()

// Block info
// Nonce - a hash that when combined with the minhash proofs thath
// The block has gone through proof of work(POW)
// 8 bytes => 64bits