pragma solidity >=0.4.22 <0.9.0;

// they can't inherit from other

interface IFaucet {
    function addFunds() external payable;
    function withdraw(uint withdrawAmount) external;   
}